/**
 * OpenID Connect 1.0 is a simple identity layer on top of the OAuth 2.0 protocol.
 * It enables Clients to verify the identity of the End-User based on the authentication performed by an Authorization Server,
 * as well as to obtain basic profile information about the End-User in an interoperable and REST-like manner.
 *
 * @author yadong.zhang (yadong.zhang0415(a)gmail.com)
 * @date 2021/1/18 16:19
 * @version 1.0.0
 * @see <a href="https://openid.net/specs/openid-connect-core-1_0.html" target="_blank">OpenID Connect Core 1.0 incorporating errata set 1</a>
 * @since 1.0.0
 */
package com.fujieid.jap.oidc;
