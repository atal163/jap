package com.fujieid.jap.core;

/**
 * Authenticate configuration. The config parameter for each policy authenticate must be inherited from this class.
 *
 * @author yadong.zhang (yadong.zhang0415(a)gmail.com)
 * @version 1.0.0
 * @date 2021/1/12 16:15
 * @since 1.0.0
 */
public abstract class AuthenticateConfig {
}
