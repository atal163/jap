package com.fujieid.jap.core;

/**
 * JAP constant
 *
 * @author yadong.zhang (yadong.zhang0415(a)gmail.com)
 * @version 1.0.0
 * @date 2021/1/11 18:19
 * @since 1.0.0
 */
public interface JapConst {

    String SESSION_USER_KEY = "_jap:session:user";
}
