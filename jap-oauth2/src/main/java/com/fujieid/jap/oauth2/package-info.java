/**
 * OAuth is a standard protocol that allows users to authorize APIs to access the web and desktop or mobile applications.
 * Once access is granted, the authorized application can use the API on behalf of the user.
 * OAuth has also become a popular delegation authentication mechanism.
 * <p>
 * Based on OAuth 2.0 protocol (rfc6749), Jap implements OAuth 2.0 server and OAuth 2.0 client,
 * which can dock with any platform supporting OAuth 2.0 standard protocol
 *
 * @author yadong.zhang (yadong.zhang0415(a)gmail.com)
 * @date 2021/1/14 11:05
 * @version 1.0.0
 * @see <a href="https://tools.ietf.org/html/rfc6749" target="_blank">https://tools.ietf.org/html/rfc6749</a>
 * @since 1.0.0
 */
package com.fujieid.jap.oauth2;
