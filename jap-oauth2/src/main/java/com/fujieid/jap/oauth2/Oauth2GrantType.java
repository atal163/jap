package com.fujieid.jap.oauth2;

/**
 * @author yadong.zhang (yadong.zhang0415(a)gmail.com)
 * @version 1.0.0
 * @date 2021/1/14 16:14
 * @since 1.0.0
 */
public enum Oauth2GrantType {

    /**
     * Authorization Code Grant
     */
    authorization_code,
    /**
     * Resource Owner Password Credentials Grant
     */
    password,
    /**
     * Client Credentials Grant
     */
    client_credentials
}
