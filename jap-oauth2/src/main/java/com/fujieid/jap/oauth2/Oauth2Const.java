package com.fujieid.jap.oauth2;

/**
 * JAP OAuth 2.0 Constant
 *
 * @author yadong.zhang (yadong.zhang0415(a)gmail.com)
 * @version 1.0.0
 * @date 2021/1/14 15:34
 * @since 1.0.0
 */
public interface Oauth2Const {
    String SCOPE_SEPARATOR = " ";
}
