package com.fujieid.jap.oauth2;

/**
 * @author yadong.zhang (yadong.zhang0415(a)gmail.com)
 * @version 1.0.0
 * @date 2021/1/14 16:14
 * @since 1.0.0
 */
public enum Oauth2ResponseType {

    /**
     * Authorization Code Grant
     */
    code,
    /**
     * Implicit Grant
     */
    token
}
