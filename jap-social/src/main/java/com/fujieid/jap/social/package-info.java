/**
 * Integrate justauth to realize the authorized login of the third-party platform,
 * and support the automatic creation of login account
 *
 * @author yadong.zhang (yadong.zhang0415(a)gmail.com)
 * @date 2021/1/12 12:01
 * @version 1.0.0
 * @since 1.0.0
 */
package com.fujieid.jap.social;
